__author__ = 'sebast'

from threading import Thread
import requests, sys


def thread_function(port):
    try:
        r = requests.get("http://portquiz.net:{:s}".format(port), timeout=1)
    except:
        # print("Port {:s}...... Closed".format(port))
        pass
    else:
        print(port)


if __name__ == "__main__":
    for i in range(int(sys.argv[1]), int(sys.argv[2])):
        thePort = str(i)
        thread = Thread(target=thread_function, args=(thePort,))
        thread.start()
