# Description
Test open outgoing ports using [Port Quiz](http://portquiz.net).

# Usage
```bash
python porttester.py <start_port> <end_port>
```

# Examples
Test port 10 to 1024
```bash
python porttester.py 10 1024
```